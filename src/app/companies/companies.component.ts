import { Component, OnInit } from '@angular/core';
import { Company } from './company';
import { CompanyService } from './company.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html'
})
export class CompaniesComponent implements OnInit {

  public companies: Company[];
  showId: boolean;

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.companyService.getCompanies().subscribe(
      companies => this.companies = companies
    );
  }

  switchId(): void{
    this.showId = !this.showId;
  }

  delete(company: Company): void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false 
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Está seguro?',
      text: `¿Seguro que desea eliminar la company ${company.nombre}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.companyService.delete(company.idCompany).subscribe(
          response => {
            this.companies = this.companies.filter(com => com !== company)
            swalWithBootstrapButtons.fire(
              'Company Eliminada!',
              `Company ${company.nombre} eliminada con exito`,
              'success'
            )
          }
        )
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          `Has cancelado borrar la company ${company.nombre}`,
          'error'
        )
      }
    })
  }

}
