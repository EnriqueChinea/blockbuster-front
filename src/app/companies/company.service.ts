import { Injectable } from '@angular/core';
import { Company } from './company';
import { of,Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AlertService } from '../alert/alert.service';

@Injectable()
export class CompanyService { 

    private urlEndPoint: string = 'http://localhost:8090/company';

    private httpHeaders = new HttpHeaders({'Content-type': 'application/json'})

    constructor(private http: HttpClient, private router: Router,private alertService: AlertService) { }

    getCompanies(): Observable<Company[]>{
      return this.http.get<Company[]>(this.urlEndPoint).pipe(
        catchError(error => {
          if(error.status == 401){
            this.router.navigate(['/login']);
          }else{
            this.alertService.error(`Error al consultar las companies: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
          }
          return throwError(error); 
        })
      );
    }

    create(company: Company): Observable<Company>{
      return this.http.post<Company>(this.urlEndPoint,company,{headers: this.httpHeaders})
       .pipe(
         catchError(error => {
          console.error(`Error al crear la company: "${error.message}"`);
          if(error.status == 400){
            error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
              this.alertService.error(errorMessage);
            });
          }else{
            this.alertService.error(`Error al crear la company: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
          }
          return throwError(error);
         })
       )
    }
    getCompany(id: number): Observable<Company>{
      return this.http.get<Company>(`${this.urlEndPoint}/${id}`)
      .pipe(
        catchError(error => {
         console.error(`Error al obtener la company: "${error.message}"`);
         this.alertService.error(`Error al obtener la company: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
         return throwError(error);
        })
      )
    }
  
    update(company: Company): Observable<Company>{
      return this.http.put<Company>(`${this.urlEndPoint}/${company.idCompany}`, company, {headers: this.httpHeaders})
      .pipe(
        catchError(error => {
         console.error(`Error al modificar la company: "${error.message}"`);
         if(error.status == 400){
           error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
             this.alertService.error(errorMessage);
           });
         }else{
           this.alertService.error(`Error al modificar company: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
         }
         return throwError(error);
        })
      )
    }
  
    delete(id: number): Observable<Company>{
      return this.http.delete<Company>(`${this.urlEndPoint}/${id}`, {headers: this.httpHeaders})
      .pipe(
        catchError(error => {
         console.error(`Error al borrar la company: "${error.message}"`);
         if(error.status == 400){
           error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
             this.alertService.error(errorMessage);
           });
         }else{
           this.alertService.error(`Error al borrar company: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
         }
         return throwError(error);
        })
      )
    }
}