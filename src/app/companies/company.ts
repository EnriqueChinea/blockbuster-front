import { Juego } from '../juegos/juego';

export class Company{

    idCompany: number;
    cif: String;
    nombre: String;

    juegos: Juego[];
}