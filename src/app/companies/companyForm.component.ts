import { Component, OnInit } from '@angular/core';
import { Company } from './company';
import { CompanyService } from './company.service';
import { Router ,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-companyForm',
    templateUrl: './companyForm.component.html'
})
export class CompanyFormComponent implements OnInit {

    company: Company = new Company();
    title: string = "Crear Company";

    constructor(private companyService: CompanyService,
                private router: Router,
                private activatedRoute: ActivatedRoute){}

    ngOnInit(): void{
        this.cargarCompany()
    }

    cargarCompany(): void{
        this.activatedRoute.params.subscribe(params => {
          let idCompany = params['idCompany']
          if(idCompany){
            this.companyService.getCompany(idCompany).subscribe( (company) => this.company = company)
          }
        })
      }
    
      public create(): void{
        this.companyService.create(this.company)
        .subscribe(company => {
          this.router.navigate(['/company']);
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'La company se ha creado con exito',
            showConfirmButton: false,
            timer: 1500
          })
        } 
        );
      }
    
      update():void{
        this.companyService.update(this.company).subscribe(company =>{
          this.router.navigate(['/company']);
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'La company se ha modificado con exito',
            showConfirmButton: false,
            timer: 1500
          })
        })
      }
}