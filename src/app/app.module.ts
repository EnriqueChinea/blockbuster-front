import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { JuegosComponent } from './juegos/juegos.component';
import { JuegoService } from './juegos/juego.service';
import { JuegoFormComponent } from './juegos/juegoForm.component';
import { HeaderComponent } from './header/header.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from './clientes/cliente.service';
import { RouterModule, Routes} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormComponent } from './clientes/form.component';
import { FormsModule } from '@angular/forms';
import { AlertComponent } from './alert/alert.component';
import { CompaniesComponent } from './companies/companies.component';
import { CompanyService } from './companies/company.service';
import { CompanyFormComponent } from './companies/companyForm.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';

const ROUTES: Routes = [
  {path: '', redirectTo: 'juego', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'juego', component: JuegosComponent},
  {path: 'juego/form', component: JuegoFormComponent},
  {path: 'juego/form/:idJuego', component: JuegoFormComponent},
  {path: 'company', component: CompaniesComponent},
  {path: 'company/form', component: CompanyFormComponent},
  {path: 'company/form/:idCompany', component: CompanyFormComponent},
  {path: 'cliente', component: ClientesComponent},
  {path: 'cliente/form', component: FormComponent},
  {path: 'cliente/form/:idCliente', component: FormComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    JuegosComponent,
    JuegoFormComponent,
    HeaderComponent,
    ClientesComponent,
    FormComponent,
    AlertComponent,
    CompaniesComponent,
    CompanyFormComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [JuegoService,ClienteService,CompanyService,LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
