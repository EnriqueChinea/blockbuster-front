import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router ,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  cliente: Cliente = new Cliente();
  titulo: string = "Crear Cliente";

  constructor(private clienteService: ClienteService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(){
    this.cargarCliente()
  }

  cargarCliente(): void{
    this.activatedRoute.params.subscribe(params => {
      let idCliente = params['idCliente']
      if(idCliente){
        this.clienteService.getCliente(idCliente).subscribe( (cliente) => this.cliente = cliente)
      }
    })
  }

  public create(): void{
    this.clienteService.create(this.cliente)
    .subscribe(cliente => {
      this.router.navigate(['/cliente']);
      Swal.fire({
        position: 'top',
        icon: 'success',
        title: 'El cliente se ha creado con exito',
        showConfirmButton: false,
        timer: 1500
      })
    } 
    );
  }

  update():void{
    this.clienteService.update(this.cliente).subscribe(cliente =>{
      this.router.navigate(['/cliente']);
      Swal.fire({
        position: 'top',
        icon: 'success',
        title: 'El cliente se ha creado con exito',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }

}
