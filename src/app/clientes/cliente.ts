export class Cliente{
    
    idCliente: number;
    nombre: string;
    username: string;
    password: string;
    documento: string;
    correo: string;
    fechaNacimiento: string;
}