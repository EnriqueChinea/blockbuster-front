import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { of,Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AlertService } from '../alert/alert.service';

@Injectable()
export class ClienteService {

  private urlEndPoint: string = 'http://localhost:8090/cliente';

  private httpHeaders = new HttpHeaders({'Content-type': 'application/json'})

  constructor(private http: HttpClient, private router: Router,private alertService: AlertService) { }

  getClientes(): Observable<Cliente[]>{
    //return of(CLIENTES);
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Cliente[]),
      catchError(error => {
        if(error.status == 401){
          this.router.navigate(['/login']);
        }else{
          this.alertService.error(`Error al consultar los clientes: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
        }
        return throwError(error); 
       })
    );
  }

  create(cliente: Cliente): Observable<Cliente>{
    console.log(cliente);
    return this.http.post<Cliente>(this.urlEndPoint,cliente,{headers: this.httpHeaders})
    .pipe(
      catchError(error => {
       console.error(`Error al crear el cliente: "${error.message}"`);
       if(error.status == 400){
         error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
           this.alertService.error(errorMessage);
         });
       }else{
         this.alertService.error(`Error al crear el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
       }
       return throwError(error);
      })
    )
  }

  getCliente(idCliente: number): Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEndPoint}/${idCliente}`)
    .pipe(
      catchError(error => {
       console.error(`Error al obtener el cliente: "${error.message}"`);
       if(error.status == 400){
         error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
           this.alertService.error(errorMessage);
         });
       }else{
         this.alertService.error(`Error al obtener el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
       }
       return throwError(error);
      })
    )
  }

  update(cliente: Cliente): Observable<Cliente>{
    return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.idCliente}`, cliente, {headers: this.httpHeaders})
    .pipe(
      catchError(error => {
       console.error(`Error al modificar el juego: "${error.message}"`);
       if(error.status == 400){
         error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
           this.alertService.error(errorMessage);
         });
       }else{
         this.alertService.error(`Error al modificar el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
       }
       return throwError(error);
      })
    )
  }

  delete(idCliente: number): Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEndPoint}/${idCliente}`, {headers: this.httpHeaders})
    .pipe(
      catchError(error => {
       console.error(`Error al borrar el juego: "${error.message}"`);
       if(error.status == 400){
         error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
           this.alertService.error(errorMessage);
         });
       }else{
         this.alertService.error(`Error al borrar el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
       }
       return throwError(error);
      })
    )
  }

}
