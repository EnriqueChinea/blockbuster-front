import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import { Router ,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-juegoForm',
  templateUrl: './juegoForm.component.html'
})
export class JuegoFormComponent implements OnInit {

  juego: Juego = new Juego();
  title: string = "Crear Juego";
  categorias: string[] = ['Plataforma','Deporte','Carrera','Disparos','Simulacion'];

  constructor(private juegoService: JuegoService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarJuego()
  }

  cargarJuego(): void{
    this.activatedRoute.params.subscribe(params => {
      let idJuego = params['idJuego']
      if(idJuego){
        this.juegoService.getJuego(idJuego).subscribe( (juego) => this.juego = juego)
      }
    })
  }

  public create(): void{
    this.juegoService.create(this.juego)
    .subscribe(juego => {
      this.router.navigate(['/juego']);
      Swal.fire({
        position: 'top',
        icon: 'success',
        title: 'El juego se ha creado con exito',
        showConfirmButton: false,
        timer: 1500
      })
    } 
    );
  }

  update():void{
    this.juegoService.update(this.juego).subscribe(juego =>{
      this.router.navigate(['/juego']);
      Swal.fire({
        position: 'top',
        icon: 'success',
        title: 'El juego se ha modificado con exito',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }
}
