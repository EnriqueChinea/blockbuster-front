import { Injectable } from '@angular/core';
import { Juego } from './juego';
import { of,Observable , throwError} from 'rxjs';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AlertService } from '../alert/alert.service'; 

@Injectable()
export class JuegoService {

  private urlEndPoint: string = 'http://localhost:8090/juego';

  private httpHeaders = new HttpHeaders({'Content-type': 'application/json'});
  
  constructor(private http: HttpClient, private router: Router,private alertService: AlertService) { }

  getJuegos(): Observable<Juego[]>{
    //return of(JUEGOS);
    return this.http.get<Juego[]>(this.urlEndPoint).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar los juegos: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
        return throwError(error); 
       })
    );
  }
  create(juego: Juego): Observable<Juego>{
    return this.http.post<Juego>(this.urlEndPoint,juego,{headers: this.httpHeaders.append('cif','A12345678')})
     .pipe(
       catchError(error => {
        console.error(`Error al crear el juego: "${error.message}"`);
        if(error.status == 400){
          error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
            this.alertService.error(errorMessage);
          });
        }else{
          this.alertService.error(`Error al crear juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
        }
        return throwError(error);
       })
     )
  }
  getJuego(id: number): Observable<Juego>{
    return this.http.get<Juego>(`${this.urlEndPoint}/${id}`)
    .pipe(
      catchError(error => {
       console.error(`Error al obtener el juego: "${error.message}"`);
       this.alertService.error(`Error al obtener el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
       return throwError(error);
      })
    )
  }

  update(juego: Juego): Observable<Juego>{
    return this.http.put<Juego>(`${this.urlEndPoint}/${juego.idJuego}`, juego, {headers: this.httpHeaders})
    .pipe(
      catchError(error => {
       console.error(`Error al modificar el juego: "${error.message}"`);
       if(error.status == 400){
         error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
           this.alertService.error(errorMessage);
         });
       }else{
         this.alertService.error(`Error al modificar juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
       }
       return throwError(error);
      })
    )
  }

  delete(id: number): Observable<Juego>{
    return this.http.delete<Juego>(`${this.urlEndPoint}/${id}`, {headers: this.httpHeaders})
    .pipe(
      catchError(error => {
       console.error(`Error al borrar el juego: "${error.message}"`);
       if(error.status == 400){
         error.error.errorMessage.replace('[','').replace(']','').split(', ').reverse().forEach((errorMessage) => {
           this.alertService.error(errorMessage);
         });
       }else{
         this.alertService.error(`Error al borrar juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false});
       }
       return throwError(error);
      })
    )
  }
}
