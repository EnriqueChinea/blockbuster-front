import { Component, OnInit} from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-juego',
    templateUrl: './juegos.component.html'
})
export class JuegosComponent implements OnInit{
    juegos: Juego[];
    showId: boolean;

    constructor(private juegoService: JuegoService){}
    ngOnInit(){
        this.juegoService.getJuegos().subscribe(
            juegos => this.juegos = juegos
        );
    }
    switchId(): void{
        this.showId = !this.showId;
    }

    delete(juego: Juego): void{
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })
        
        swalWithBootstrapButtons.fire({
          title: '¿Está seguro?',
          text: `¿Seguro que desea eliminar el juego ${juego.titulo}?`,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Si, eliminar!',
          cancelButtonText: 'No, cancelar!',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
    
            this.juegoService.delete(juego.idJuego).subscribe(
              response => {
                this.juegos = this.juegos.filter(jue => jue !== juego)
                swalWithBootstrapButtons.fire(
                  'Juego Eliminado!',
                  `Juego ${juego.titulo} eliminado con exito`,
                  'success'
                )
              }
            )
            
          } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
          ) {
            swalWithBootstrapButtons.fire(
              'Cancelado',
              `Has cancelado borrar el juego ${juego.titulo}`,
              'error'
            )
          }
        })
      }
}
