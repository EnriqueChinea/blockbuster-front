import { Company } from '../companies/company';

export class Juego{
    idJuego: number;
    titulo: string;
    categoria: string;
    pegi: number;
    fechaLanzamiento: string;

    companies: Company[];
}