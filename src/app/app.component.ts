import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titles: string[] = ['blockbuster-front','blockbuster-front2','blockbuster-front3','blockbuster-front'];
}
